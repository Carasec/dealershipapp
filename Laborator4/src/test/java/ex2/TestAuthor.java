package ex2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestAuthor {
    @Test
    void TestConstructor() {
        Author author = new Author("Robert Kyosaki", "robertkyosaki@yahoo.com", 'm');
        assertEquals("Robert Kyosaki", author.getName());
        assertEquals("robertkyosaki@yahoo.com", author.getEmail());
        assertEquals('m', author.getGender());
    }
}

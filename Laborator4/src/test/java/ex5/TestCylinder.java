package ex5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCylinder {

    @Test
    void TestConstructor() {
        Cylinder c = new Cylinder(4);
        assertEquals(4, c.getRadius());
    }

    @Test
    void TestConstructor2() {
        Cylinder c2 = new Cylinder(4, 5);
        assertEquals(4, c2.getRadius());
        assertEquals(5, c2.getHeight());

    }

    @Test
    void TestGetArea() {
        Cylinder c = new Cylinder();
        assertEquals(12.563185307179587, c.getArea(), 0.001);
    }

    @Test
    void TestGetVolume() {
        Cylinder c = new Cylinder();
        assertEquals(3.14, c.getVolume(), 0.001);
    }


}

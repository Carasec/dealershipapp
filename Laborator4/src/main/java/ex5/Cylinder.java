package ex5;

import ex1.Circle;

public class Cylinder extends Circle {
    private double height = 1;

    public Cylinder() {

    }

    public Cylinder(double radius) {
        super(radius);

    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return super.getArea() * this.height;
    }

    public double getArea() {
        return (2 * Math.PI * getRadius() * getHeight()) + 2 * super.getArea();
    }
}

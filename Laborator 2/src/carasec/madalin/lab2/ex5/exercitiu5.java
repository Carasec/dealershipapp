package carasec.madalin.lab2.ex5;

import java.util.Random;


public class exercitiu5 {
    public static void main(String[] args) {
        int[] num = new int[10];
        Random ran = new Random();
        for (int i = 0; i < 10; i++)
            num[i] = ran.nextInt(30);
        System.out.print("Enter the elements:");
        for (int i = 0; i < 10; i++)
            System.out.print(num[i] + " ");
        System.out.print("\n");
        for (int i = 0; i < num.length - 1; i++)
            for (int j = i + 1; j < 10; j++)
                if (num[i] > num[j]) {
                    int aux = num[i];
                    num[i] = num[j];
                    num[j] = aux;
                }
        System.out.print("Sorted vector:");
        for (int i = 0; i < 10; i++)
            System.out.print(num[i] + " ");


    }


}


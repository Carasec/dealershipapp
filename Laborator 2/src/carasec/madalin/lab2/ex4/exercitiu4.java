package carasec.madalin.lab2.ex4;

import java.util.Scanner;

public class exercitiu4 {
    public static void main(String[] args) {
        int i;
        int max = 0;
        int[] num = new int[10];
        Scanner in = new Scanner(System.in);

        System.out.print("Enter the number of elements:");
        int n = in.nextInt();
        System.out.print("Enter the elements:");
        for (i = 0; i < n; i++) {
            System.out.print("v[" + i + "]=");
            int y = in.nextInt();
            num[i] = y;
        }
        for (i = 0; i < n; i++) {
            if (max < num[i]) {
                max = num[i];
            }
        }
        System.out.print("Maximum is:" + max);
    }
}

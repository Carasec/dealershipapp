package carasec.madalin.lab2.ex3;

import java.util.Scanner;

public class exercitiu3 {
    public static void main(String[] args) {

        System.out.print("Enter 2 numbers::");
        Scanner in = new Scanner(System.in);
        int low = in.nextInt();
        int high = in.nextInt();
        int nr = 0;
        if (low > 0)
            while (low < high) {
                boolean flag = false;
                for (int i = 2; i <= low / 2; ++i) {
                    if (low % i == 0) {
                        flag = true;
                        break;

                    }
                }
                if (!flag && low != 0 && low != 1)
                    System.out.print(low + " ");
                ++low;

            }
    }

}

package Ex4;

public class FireSensor {
    AlarmUnit alarm = new AlarmUnit();
    GSMUnit gsmPhone = new GSMUnit();

    public String activate(FireEvent fireEvent) {
        String sensorStatus;

        if (fireEvent.isSmoke()) {
            alarm.startAlarm();
            gsmPhone.callOwner();
            sensorStatus = "ON";
        } else {
            alarm.stopAlarm();
            gsmPhone.stopCalling();
            sensorStatus = "OFF";
        }

        return "-------------------------------\n"+
                "FireSensor is: " + sensorStatus+ "\n\t" +
                alarm.toString() + "\n\t" +
                gsmPhone.toString() + "\n"+
                "-----------------------------\n\n";
    }
}

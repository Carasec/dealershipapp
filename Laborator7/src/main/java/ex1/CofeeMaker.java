package ex1;

public class CofeeMaker {
    private final int NrOfCofees;

    public CofeeMaker(int nrOfCofees) {
        this.NrOfCofees = nrOfCofees;
    }

    Cofee makeCofee() throws Exception {
        System.out.println("Make a coffe");
        int t = (int) (Math.random() * 100);
        int c = (int) (Math.random() * 100);
        if (Cofee.getNrOfInstances() == NrOfCofees) {
            throw new Exception("Too many cofees");

        }
        Cofee cofee = new Cofee(t, c);
        return cofee;
    }


}

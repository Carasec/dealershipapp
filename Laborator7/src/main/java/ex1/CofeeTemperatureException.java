package ex1;

public class CofeeTemperatureException extends Throwable {
    int t;

    public CofeeTemperatureException(int t, String msg) {
        super(msg);
        this.t = t;
    }

    int getTemp() {
        return t;
    }
}

package ex1;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestRobot {
    @Test
    void TestConstructor(){
        Robot robot=new Robot();
        assertEquals(1, robot.getX());
    }
    @Test
    void testChangedWhenValueIsHigherThan1(){
        Robot robot=new Robot();
        assertEquals(1, robot.getX());
        robot.change(10);
        assertEquals(11,robot.getX());
    }
    @Test
    void testChangedWhenValueIsLowerThan1(){
        Robot robot=new Robot();
        assertEquals(1,robot.getX());
        robot.change(0);
        assertEquals(1, robot.getX());
    }
}

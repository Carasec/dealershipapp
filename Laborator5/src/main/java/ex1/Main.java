package ex1;

public class Main {
    public static void main(String[] args) {
        Shape[] array = new Shape[4];
        array[0] = new Circle(3);
        array[1] = new Rectangle(3.2, 5.6);
        array[2] = new Square(4);
        array[3] = new Rectangle(4.3, 6.4);
        array[4] = new Circle(10);

        for (int i = 0; i < array.length; i++) {
            System.out.println(array[1].GetArea());
            System.out.println(array[i].GetPerimeter());
        }
    }
}
